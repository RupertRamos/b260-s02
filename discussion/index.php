
<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02: Repetition Control Structures and Array Manipulation</title>
</head>
<body>

	<h1>Repetition Control Structures</h1>

	<h3>While Loop</h3>
	<?php whileLoop(); ?>


	<h3>Do-While Loop</h3>
	<?php doWhileLoop(); ?>

	<h3>For Loop</h3>
	<?php forLoop(); ?>

	<h3>Continue and Break</h3>
	<?php modifiedForLoop(); ?>


	<h1>Array and Array Manipulation</h1>

	<h2>Types of Array</h2>

	<h3>Simple Array</h3>

	<ul>
		<?php foreach($computerBrands as $brand) {?>
				<li><?php echo $brand; ?></li>
		<?php } ?>
	</ul>

	<h3>Associative Array</h3>

	<ul>
		<?php foreach($gradePeriods as $period => $grade) {?>
			<li>Grade in <?= $period ?> is <?= $grade ?></li>
		<?php } ?>
	</ul>

	<h3>Multidimensional Array</h3>

	<ul>
		<?php 
			foreach($heroes as $team) {
				foreach($team as $member) {
			?>
				<li><?php echo $member; ?></li>
			<?php }
			}
		?>
	</ul>

	<h3>Two-Dimensional Assoc. Array</h3>

	<ul>
		<?php 
			foreach($ironManPowers as $label => $powerGroup) {
				foreach($powerGroup as $power) {
			?>
				<li><?= "$label: $power"; ?></li>
			<?php }
			} 
		?>
	</ul>


	<h2>Array Functions</h2>

	<h3>Sorting</h3>

	<pre><?php print_r($sortedBrands); ?></pre>

	<h3>Sorting (Reverse)</h3>

	<pre><?php print_r($reverseSortedBrands); ?></pre>

	<h3>Append</h3>

	<?php array_push($computerBrands, 'Apple'); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<?php array_unshift($computerBrands, 'Dell'); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Remove</h3>

	<?php array_pop($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<?php array_shift($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>


	<h3>Count</h3>
	<pre><?php echo count($computerBrands); ?></pre>

	<h3>In Array</h3>
	<p><?php echo searchBrand($computerBrands, 'HP'); ?></p>


	<h3>Reverse (not A-Z sorting)</h3>
	<pre><?php print_r($reverseGradePeriods); ?></pre>


</body>
</html>