<?php require_once "./code.php"; ?>

<!DOCTYPE html>

<html>

    <head>

        <title>S02: Repetition Control Structures and Array Manipulation (Activity)</title>
    
    </head>

    <body>

        <h1>Divisibles of Five</h1>

        <?php printDivisiblesOfFive(); ?>

        <h1>Array Manipulation</h1>

        <?php $students = []; ?>

        <?php array_push($students, 'John Smith'); ?>

        <p><?php var_dump($students); ?></p>

        <p><?php echo count($students); ?></p>

        <?php array_push($students, 'Jane Smith'); ?>

        <p><?php var_dump($students); ?></p>

        <p><?php echo count($students); ?></p>

        <?php array_shift($students); ?>

        <p><?php var_dump($students); ?></p>

        <p><?php echo count($students); ?></p>

    </body>

</html>